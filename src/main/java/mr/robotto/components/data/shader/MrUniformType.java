/*
 * MrRobotto Engine
 * Copyright (c) 2015, Aarón Negrín, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package mr.robotto.components.data.shader;

public enum MrUniformType {
    MATRIX_MODEL,
    MATRIX_VIEW,
    MATRIX_PROJECTION,
    MATRIX_MODEL_VIEW,
    MATRIX_VIEW_PROJECTION,
    MATRIX_MODEL_VIEW_PROJECTION,
    MATRIX_TRANSP_MODEL_VIEW,
    MATRIX_INVERSE_TRANSP_MODEL_VIEW,
    MATRIX_TEXTURE_0,
    MATRIX_TEXTURE_1,
    MATRIX_TEXTURE_2,
    MATRIX_TEXTURE_3
}