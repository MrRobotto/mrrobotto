/*
 * MrRobotto Engine
 * Copyright (c) 2015, Aarón Negrín, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package mr.robotto.components.renderer.renderinterfaces;

public interface MrBindable<T> extends MrInitializable<T> {
    public boolean isBinded();

    public void bind();

    public void unbind();
}
